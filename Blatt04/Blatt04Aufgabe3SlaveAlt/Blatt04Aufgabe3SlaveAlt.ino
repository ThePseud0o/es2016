#include <Wire.h>
//#include "duetimer.h"

int x, ref, pos;
double x2;

void setup() {
  Wire.begin(8);
  Wire.onRequest(getAngle);
  Serial.begin(9600);
}

void loop() {
}


void getAngle() {
  ref = analogRead(4);
  x = analogRead(5) - ref;
  x = map(x, -512, 511, -90, 90);
  
  Serial.println(x);

  pos += x;
  if(pos >= 180){
       pos = 180;
    } else if(pos <= 0){
       pos = 0;
    }

  Wire.write(pos);
}


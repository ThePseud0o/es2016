#include <Wire.h>

int led = 0;
int timer  = 0;
double pos = 90;
int ledPin = 13;

void setup() {
  Serial.begin(9600);
  Wire.begin();
}

void loop() {
  Wire.beginTransmission(8);
  int b = Wire.requestFrom(8, 1);
  Wire.endTransmission();
  pos += b*0,001;
  if(pos >= 180){
     pos = 180;
//     blinkLed();
  } else if(pos <= 0){
     pos = 0;
//     blinkLed();
  }
  myservo.write(pos);
}

void blinkLed() {
  for(int i = 0; i < 3; i++) {
   digitalWrite(ledPin, HIGH);
   delay(100);
   digitalWrite(ledPin, LOW);
   delay(50);
  }
}

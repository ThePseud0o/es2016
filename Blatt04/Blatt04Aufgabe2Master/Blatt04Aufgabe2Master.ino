#include <Wire.h>

#include "duetimer.h"

int led = 0;
int timer  = 0;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  configureTimer(7, switchLed, 1);
  startTimer(7);
}

void loop() {
}

void switchLed() {
  if(timer++ % 2) {
    Wire.beginTransmission(8);
    Wire.write(led);
    Wire.endTransmission();
    
    Wire.beginTransmission(8);
    if(Wire.requestFrom(8, 1)) {
      int b = Wire.read();
      Serial.println(b == led);
    }
    
    Wire.endTransmission();
    
    led = ++led % 2;
  }
}

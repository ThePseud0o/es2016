#include <Wire.h>

int x, ref;

void setup() {
  Wire.begin(8);
  Wire.onRequest(getAngle);
  Serial.begin(9600);
}

void loop() {
}


void getAngle() {
  ref = analogRead(4);
  x = analogRead(5) - ref;
  x = x*220000/1023;
  
  Serial.println(x);
  Wire.write(x);
  }
}


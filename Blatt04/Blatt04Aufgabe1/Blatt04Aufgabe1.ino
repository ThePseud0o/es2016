#include "duetimer.h"

int ledPin = 13;
int brightness = 0;
int dir = 1;

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial3.begin(115200);
  Serial.begin(9600);
  configureTimer(7, led, 51);
  startTimer(7);
}

void loop() {
  analogWrite(ledPin, brightness);
}

void led() {
  if (brightness == 0) {
    if (Serial3.available()) {
      int input = Serial3.read();
      if (input == 1) {
        brightness += dir;
      }
    }
  } else {
    brightness += dir;
    if (brightness == 0 || brightness == 255) {
      dir *= -1;
      Serial3.write(42);
    }
  }
}

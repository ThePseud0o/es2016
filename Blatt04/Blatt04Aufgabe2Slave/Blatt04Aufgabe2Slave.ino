#include <Wire.h>

#include "duetimer.h"

int led = 0;
int ledPin = 13;

void setup() {
  Wire.begin(8);
  Wire.onReceive(powerLed);
  Wire.onRequest(debug);
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop() {
}

void powerLed(int count) {
  while(Wire.available()) {
    led = Wire.read();
    digitalWrite(ledPin, led);
  }
}

void debug() {
  Wire.write(led);
}


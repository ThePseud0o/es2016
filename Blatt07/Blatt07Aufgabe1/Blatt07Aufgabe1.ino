#include <SD.h>
#include <SPI.h>

#define DISPLAY_HEIGHT 48
#define DISPLAY_WIDTH 84
#define CHAR_SPACE 6

unsigned char font[95][6] =
{
  { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, // space
  { 0x00, 0x00, 0x5F, 0x00, 0x00, 0x00 }, // !
  { 0x00, 0x07, 0x00, 0x07, 0x00, 0x00 }, // "
  { 0x14, 0x7F, 0x14, 0x7F, 0x14, 0x00 }, // #
  { 0x24, 0x2A, 0x7F, 0x2A, 0x12, 0x00 }, // $
  { 0x23, 0x13, 0x08, 0x64, 0x62, 0x00 }, // %
  { 0x36, 0x49, 0x55, 0x22, 0x50, 0x00 }, // &
  { 0x00, 0x00, 0x07, 0x00, 0x00, 0x00 }, // '
  { 0x00, 0x1C, 0x22, 0x41, 0x00, 0x00 }, // (
  { 0x00, 0x41, 0x22, 0x1C, 0x00, 0x00 }, // )
  { 0x0A, 0x04, 0x1F, 0x04, 0x0A, 0x00 }, // *
  { 0x08, 0x08, 0x3E, 0x08, 0x08, 0x00 }, // +
  { 0x00, 0x50, 0x30, 0x00, 0x00, 0x00 }, // ,
  { 0x08, 0x08, 0x08, 0x08, 0x08, 0x00 }, // -
  { 0x00, 0x60, 0x60, 0x00, 0x00, 0x00 }, // .
  { 0x20, 0x10, 0x08, 0x04, 0x02, 0x00 }, // slash
  { 0x3E, 0x51, 0x49, 0x45, 0x3E, 0x00 }, // 0
  { 0x00, 0x42, 0x7F, 0x40, 0x00, 0x00 }, // 1
  { 0x42, 0x61, 0x51, 0x49, 0x46, 0x00 }, // 2
  { 0x21, 0x41, 0x45, 0x4B, 0x31, 0x00 }, // 3
  { 0x18, 0x14, 0x12, 0x7F, 0x10, 0x00 }, // 4
  { 0x27, 0x45, 0x45, 0x45, 0x39, 0x00 }, // 5
  { 0x3C, 0x4A, 0x49, 0x49, 0x30, 0x00 }, // 6
  { 0x03, 0x71, 0x09, 0x05, 0x03, 0x00 }, // 7
  { 0x36, 0x49, 0x49, 0x49, 0x36, 0x00 }, // 8
  { 0x06, 0x49, 0x49, 0x29, 0x1E, 0x00 }, // 9
  { 0x00, 0x36, 0x36, 0x00, 0x00, 0x00 }, // :
  { 0x00, 0x56, 0x36, 0x00, 0x00, 0x00 }, // ;
  { 0x08, 0x14, 0x22, 0x41, 0x00, 0x00 }, // <
  { 0x14, 0x14, 0x14, 0x14, 0x14, 0x00 }, // =
  { 0x00, 0x41, 0x22, 0x14, 0x08, 0x00 }, // >
  { 0x02, 0x01, 0x51, 0x09, 0x06, 0x00 }, // ?
  { 0x32, 0x49, 0x79, 0x41, 0x3E, 0x00 }, // @
  { 0x7E, 0x11, 0x11, 0x11, 0x7E, 0x00 }, // A
  { 0x7F, 0x49, 0x49, 0x49, 0x36, 0x00 }, // B
  { 0x3E, 0x41, 0x41, 0x41, 0x22, 0x00 }, // C
  { 0x7F, 0x41, 0x41, 0x41, 0x3E, 0x00 }, // D
  { 0x7F, 0x49, 0x49, 0x49, 0x41, 0x00 }, // E
  { 0x7F, 0x09, 0x09, 0x09, 0x01, 0x00 }, // F
  { 0x3E, 0x41, 0x41, 0x49, 0x7A, 0x00 }, // G
  { 0x7F, 0x08, 0x08, 0x08, 0x7F, 0x00 }, // H
  { 0x00, 0x41, 0x7F, 0x41, 0x00, 0x00 }, // I
  { 0x20, 0x40, 0x41, 0x3F, 0x01, 0x00 }, // x
  { 0x7F, 0x08, 0x14, 0x22, 0x41, 0x00 }, // K
  { 0x7F, 0x40, 0x40, 0x40, 0x40, 0x00 }, // L
  { 0x7F, 0x02, 0x0C, 0x02, 0x7F, 0x00 }, // M
  { 0x7F, 0x04, 0x08, 0x10, 0x7F, 0x00 }, // N
  { 0x3E, 0x41, 0x41, 0x41, 0x3E, 0x00 }, // O
  { 0x7F, 0x09, 0x09, 0x09, 0x06, 0x00 }, // P
  { 0x3E, 0x41, 0x51, 0x21, 0x5E, 0x00 }, // Q
  { 0x7F, 0x09, 0x19, 0x29, 0x46, 0x00 }, // R
  { 0x26, 0x49, 0x49, 0x49, 0x32, 0x00 }, // S
  { 0x01, 0x01, 0x7F, 0x01, 0x01, 0x00 }, // T
  { 0x3F, 0x40, 0x40, 0x40, 0x3F, 0x00 }, // U
  { 0x1F, 0x20, 0x40, 0x20, 0x1F, 0x00 }, // V
  { 0x3F, 0x40, 0x38, 0x40, 0x3F, 0x00 }, // W
  { 0x63, 0x14, 0x08, 0x14, 0x63, 0x00 }, // X
  { 0x07, 0x08, 0x70, 0x08, 0x07, 0x00 }, // Y
  { 0x61, 0x51, 0x49, 0x45, 0x43, 0x00 }, // Z
  { 0x00, 0x7F, 0x41, 0x41, 0x00, 0x00 }, // [
  { 0x02, 0x04, 0x08, 0x10, 0x20, 0x00 }, // backslash
  { 0x00, 0x41, 0x41, 0x7F, 0x00, 0x00 }, // ]
  { 0x04, 0x02, 0x01, 0x02, 0x04, 0x00 }, // ^
  { 0x40, 0x40, 0x40, 0x40, 0x40, 0x00 }, // _
  { 0x00, 0x01, 0x02, 0x04, 0x00, 0x00 }, // `
  { 0x20, 0x54, 0x54, 0x54, 0x78, 0x00 }, // a
  { 0x7F, 0x48, 0x44, 0x44, 0x38, 0x00 }, // b
  { 0x38, 0x44, 0x44, 0x44, 0x20, 0x00 }, // c
  { 0x38, 0x44, 0x44, 0x48, 0x7F, 0x00 }, // d
  { 0x38, 0x54, 0x54, 0x54, 0x18, 0x00 }, // e
  { 0x08, 0x7E, 0x09, 0x01, 0x02, 0x00 }, // f
  { 0x08, 0x54, 0x54, 0x54, 0x3C, 0x00 }, // g
  { 0x7F, 0x08, 0x04, 0x04, 0x78, 0x00 }, // h
  { 0x00, 0x48, 0x7D, 0x40, 0x00, 0x00 }, // i
  { 0x20, 0x40, 0x44, 0x3D, 0x00, 0x00 }, // x
  { 0x7F, 0x10, 0x28, 0x44, 0x00, 0x00 }, // k
  { 0x00, 0x41, 0x7F, 0x40, 0x00, 0x00 }, // l
  { 0x7C, 0x04, 0x78, 0x04, 0x78, 0x00 }, // m
  { 0x7C, 0x08, 0x04, 0x04, 0x78, 0x00 }, // n
  { 0x38, 0x44, 0x44, 0x44, 0x38, 0x00 }, // o
  { 0x7C, 0x14, 0x14, 0x14, 0x08, 0x00 }, // p
  { 0x08, 0x14, 0x14, 0x18, 0x7C, 0x00 }, // q
  { 0x7C, 0x08, 0x04, 0x04, 0x08, 0x00 }, // r
  { 0x48, 0x54, 0x54, 0x54, 0x20, 0x00 }, // s
  { 0x04, 0x3F, 0x44, 0x40, 0x20, 0x00 }, // t
  { 0x3C, 0x40, 0x40, 0x20, 0x7C, 0x00 }, // u
  { 0x1C, 0x20, 0x40, 0x20, 0x1C, 0x00 }, // v
  { 0x3C, 0x40, 0x30, 0x40, 0x3C, 0x00 }, // w
  { 0x44, 0x28, 0x10, 0x28, 0x44, 0x00 }, // x
  { 0x0C, 0x50, 0x50, 0x50, 0x3C, 0x00 }, // y
  { 0x44, 0x64, 0x54, 0x4C, 0x44, 0x00 }, // z
  { 0x00, 0x08, 0x36, 0x41, 0x00, 0x00 }, // {
  { 0x00, 0x00, 0x7F, 0x00, 0x00, 0x00 }, // |
  { 0x00, 0x41, 0x36, 0x08, 0x00, 0x00 }, // }
  { 0x10, 0x08, 0x08, 0x10, 0x08, 0x00 } // ~
};



int RSTpin = 6;
int dc = 5;
int LEDpin = 2;
int sce = 10;
byte buf[6 * 84];

int sdSlaveSelectPin = 4;

File root;


void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);

  SD.begin(sdSlaveSelectPin);
}

boolean v;
String input = "";

void loop() {
  if (Serial1.available()) {
    //    input = Serial1.readString();
    char newChar = (char)Serial1.read();
    if (newChar == '$') {
      if (input.startsWith("$GPGGA") && input.substring(input.length()-4, input.length()-2) == checkSum(input)) {
   //     if(hasGpsFix(input)) {
          writeToSd(getTimeString(input) + ": " + getLat(input) + ", " + getLon(input) + ", " + getSatCount(input));
   //     }
      }
      input = "";
    }
    input += newChar;
  }
}

//$GPRMC,000931.799,V,,,,,0.00,0.00,060180,,,N*4E
//
//$GPVTG,0.00,T,,M,0.00,N,0.00,K,N*32
//
//$GPGGA,000932.799,,,,,0,0,,,M,,M,,*47
//
//$GPGSA,A,1,,,,,,,,,,,,,,,*1E
//
//$GPGSV,1,1,00*79
//
//$GPRMC,000932.799,V,,,,,0.00,0.00,060180,,,N*4D
//
//$GPVTG,0.00,T,,M,0.00,N,0.00,K,N*32
//
//$GPGGA,000933.799,,,,,0,0,,,M,,M,,*46
//
//$GPGSA,A,1,,,,,,,,,,,,,,,*1E
//
//$GPRMC,000933.799,V,,,,,0.00,0.00,060180,,,N*4C
//
//$GPVTG,0.00,T,,M,0.00,N,0.00,K,N*32
//
//$GPGGA,000934.799,,,,,0,0,,,M,,M,,*41
//
//$GPGSA,A,1,,,,,,,,,,,,,,,*1E
//
//$GPRMC,000934.799,V,,,,,0.00,0.00,060180,,,N*4B

//String foo = "$GPGGA,064951.000,2307.1256,N,12016.4438,E,1,8,0.95,39.9,M,17.8,M,,*65";


//float getLat(String in) {
// int start = in.indexOf("$GPGGA,");
// String s = in.substring(start).substring(in.indexOf("TODO"));
// float lat = in.substring(pos);
//}

//float getLon(String in) {

//}

void writeToSd(String input) {
  File dataFile = SD.open("gps.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.println(input);
    dataFile.close();
    Serial.println(input);
  }
  else {
    Serial.println("error opening datalog.txt");
  }
}

//js:start=0; end=0; b=0; for(i = 0; i < 2; i++) { b = a.indexOf(",", b+1);} alert(a.substring(b+1,a.indexOf(",",b+1)));
// Liefert einen zwischen zwei Kommata eingeschlossenen String zurueck
String substringBetweenCommas(String in, int comma) {
  int start = 0;
  int end = 0;
  int pos = 0;
  for (int i = 0; i < comma; i++) {
    pos = in.indexOf(",", pos + 1);
  }
  return in.substring(pos + 1, in.indexOf(",", pos + 1));
}

String getTime(String in) {
  return substringBetweenCommas(in, 1);
}

// needs date from $RMC-Message
String getTimeString(String in) {
  return getTime(in).substring(0, 2) + ":" + getTime(in).substring(2, 4) + ":" + getTime(in).substring(4, 6);
}

float getLat(String in) {
  String out = substringBetweenCommas(in, 2);
  int d = out.substring(0, 2).toInt();
  float m = out.substring(2).toFloat();
  float lat = d + m / 60.0f;
  if (substringBetweenCommas(in, 3) == "S") {
    lat = lat * -1;
  }
  return lat;
}

float getLon(String in) {
  String out = substringBetweenCommas(in, 4);
  int d = out.substring(0, 3).toInt();
  float m = out.substring(3).toFloat();
  float lon = d + m / 60.0f;
  if (substringBetweenCommas(in, 5) == "W") {
    lon = lon * -1;
  }
  return lon;
}

boolean hasGpsFix(String in) {
  return substringBetweenCommas(in, 6).toInt();
}

int getSatCount(String in) {
  return substringBetweenCommas(in, 7).toInt();
}


String checkSum(String input) {
//  Serial.println(input);
  if (input.length() < 4) {
    return "00";
  } else {
    char check = 0;
    for (int i = 1; i < input.length() - 5; i++) {
//      Serial.print(input.charAt(i));
      check = char(check ^ input.charAt(i));
    }
//    Serial.println();
    return String(check, HEX);
  }
}









void resetDisplay() {
  digitalWrite(RSTpin, LOW);
  delay(500);
  digitalWrite(RSTpin, HIGH);
  //  emptyBuffer();
  //  writeBuffer();
}

void initializeDisplay() {
  digitalWrite(dc, LOW);
  SPI.transfer(sce, 0x21);
  SPI.transfer(sce, 0x14);
  SPI.transfer(sce, 0xF0); //contrast
  SPI.transfer(sce, 0x20);
  SPI.transfer(sce, 0x0C);

  SPI.transfer(sce, 0b01000000);
  SPI.transfer(sce, 0b10000000);
  digitalWrite(dc, HIGH);


}

void writeDD(byte data) {
  digitalWrite(sce, LOW);
  SPI.transfer(sce, data);
}

void emptyBuffer() {
  for (int i = 0; i < 6 * 84; i++) {
    buf[i] = 0;
  }
}

void setPixel(int x, int y, boolean val) {
  setPixelWithoutRefresh(x, y, val);
  writeBuffer();
}

void setPixelWithoutRefresh(int x, int y, boolean val) {
  if (x < 84 && y < 48 && x >= 0 && y >= 0) {
    if (val) {
      buf[(y / 8 * 84) + x] = bitSet(buf[(y / 8 * 84) + x], (y % 8));
    } else {
      buf[(y / 8 * 84) + x] = bitClear(buf[(y / 8 * 84) + x], (y % 8));
    }
  }
}

void writeBuffer() {
  for (int i = 0; i < 6 * 84; i++) {
    writeDD(buf[i]);
  }
}

void testDisplay() {
  for (int i = 0; i < 84; i++) {
    for (int x = 0; x < 48; x++) {
      setPixelWithoutRefresh(i, x, v);
    }
    writeBuffer();
    delay(20);
  }
}

void printChar(int x, int y, char value) {
  value -= 0x20;
  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 8; j++) {
      setPixelWithoutRefresh(x + i, y + j, bitRead(font[value][i], j));
    }
  }
  writeBuffer();
}

void printTestChar() {
  printChar(39, 20, '@');
}

void printLine(int y, String in) {
  for (int i = 0; i < in.length(); i++) {
    printChar(i * CHAR_SPACE, y, in.charAt(i));
  }
}

void printLineCentered(int y, String in) {
  int width = in.length() * CHAR_SPACE;
  for (int i = 0; i < in.length(); i++) {
    printChar(DISPLAY_WIDTH / 2 - width / 2 + i * CHAR_SPACE, y, in.charAt(i));
    //        Serial.println(""+(int)(DISPLAY_WIDTH/2-width/2+i*CHAR_SPACE));
  }
}

void showNames() {
  emptyBuffer();
  printLineCentered(11, "Arne");
  printLineCentered(20, "Buengener");
  printLineCentered(29, "1234567");
  delay(5000);
  emptyBuffer();
  printLineCentered(11, "xan");
  printLineCentered(20, "Gaetcke");
  printLineCentered(29, "1234567");
  delay(5000);
}

void ls() {
  root = SD.open("/");
  root.rewindDirectory();
  File file = root.openNextFile();
  while (file) {
    Serial.println(file.name());
    file.close();
    file = root.openNextFile();
  }
  root.close();
}

boolean fileTest(String path) {
  path = "/" + path;
  char filename[path.length() + 1];
  path.toCharArray(filename, sizeof(filename));
  root = SD.open("/");
  boolean ret = SD.exists(filename);
  root.close();
  return ret;
}

void show(String path) {
  if (path.endsWith(".IMG")) {
    showImg(path);
  } else if (path.endsWith(".TXT")) {
    showText(path);
  }
  else {
    printLineCentered(DISPLAY_HEIGHT / 2 - 4, "Error!");
    Serial.println("File is not displayable.");
  }
}

void showImg(String path) {
  emptyBuffer();
  path = "/" + path;
  char filename[path.length() + 1];
  path.toCharArray(filename, sizeof(filename));
  if (SD.exists(filename)) {
    File f = SD.open(filename);
    int width;
    int height;
    String text = "";
    char lastChar;

    while (lastChar != '\n') {
      text += lastChar;
      lastChar = (char)f.read();
    }
    text = text.substring(1);
    width = text.toInt();
    height = (int)text.substring(text.indexOf(',') + 1).toInt();
    //    Serial.println("" + (String)width + "x" + (String)height);
    Serial.println("Showing image");

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        char p = f.read();
        int val;
        if (p == '1') {
          val = 1;
        } else {
          val = 0;
        }
        setPixelWithoutRefresh(DISPLAY_WIDTH / 2 - width / 2 + x, DISPLAY_HEIGHT / 2 - height / 2 + y, val);
        f.read();
        //        Serial.println(p);
      }
    }
    writeBuffer();
    //read dimensions
    //centering
    f.close();
  }
}


void showText(String path) {
  emptyBuffer();
  path = "/" + path;
  char filename[path.length() + 1];
  path.toCharArray(filename, sizeof(filename));
  if (SD.exists(filename)) {
    File f = SD.open(filename);
    String text = "";
    char lastChar;
    int lines = 0;
    if (f.size() > 6 * 14) {
      Serial.println("Too many characters");
    }
    for (int i = 0; i < f.size(); i++) {
      lastChar = (char)f.read();
      text = text + lastChar;
      //  if(text.length() == 14 || lastChar == '\n') {
      if (text.length() == 14) {
        printLineCentered(lines * 8, text);
        text = "";
        lines++;
      }
    }
    f.close();
  }
}

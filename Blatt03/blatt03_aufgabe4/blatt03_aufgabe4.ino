int ledPin = 13;

int incomingByte = 0;

void setup() {
  pinMode(ledPin, OUTPUT);

  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    if (incomingByte == 48) {
      digitalWrite(ledPin, LOW);
    } else if (incomingByte == 49) {
      digitalWrite(ledPin, HIGH);
    }
  }
}

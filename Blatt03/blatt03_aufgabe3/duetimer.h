#ifndef DUETIMER_H
#define DUETIMER_H

#include <inttypes.h>

bool configureTimer(const uint8_t timer_id, void (*timer_callback)(void),
                    const uint32_t timer_hz);

bool startTimer(const uint8_t timer_id);

bool stopTimer(const uint8_t timer_id);

#endif // DUETIMER_H

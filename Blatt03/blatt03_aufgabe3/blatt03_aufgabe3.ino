#include "duetimer.h"
#include <Servo.h>

int z, x, ref;
Servo myservo;
int pos = 90;
int reduction = 0;
int stepsize = 10;
int ledPin = 13;


void setup() {
  Serial.begin(9600);
  configureTimer(7, readAndPrint, 10);
  startTimer(7);
  
  pinMode(ledPin, OUTPUT);
  
  myservo.attach(2);
  myservo.write(pos);
  delay(90*15);
  blinkLed();
}

void loop() {
  if(pos >= 180){
     pos = 180;
     blinkLed();
  } else if(pos <= 0){
     pos = 0;
     blinkLed();
  }
  myservo.write(pos);
}

void readAndPrint() {
  ref = analogRead(2);
  z = analogRead(0) - ref;
  z = map(z, -512, 511, -90, 90);
  x = analogRead(1) - ref;
  x = map(x, -512, 511, -90, 90);
  Serial.println("x: " + (String)x + ", z: " + (String)z);
  pos += x;
}

void blinkLed() {
  for(int i = 0; i < 3; i++) {
   digitalWrite(ledPin, HIGH);
   delay(100);
   digitalWrite(ledPin, LOW);
   delay(50);
  }
}

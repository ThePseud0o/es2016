#include <Servo.h>

Servo myservo;
int pos = 90;
int reduction = 0;
int stepsize = 10;

void setup() {
  myservo.attach(2);
  myservo.write(pos);
  delay(90*15);
  Serial.begin(9600);
}

void loop() {
  for (pos; pos <= 180-reduction; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);           // waits 15ms for the servo to reach the position
    Serial.println((String)pos);
  }
  for (pos; pos >= 0+reduction; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
    Serial.println((String)pos);
  }
  reduction += stepsize;
  if(reduction > 90){
    reduction = 0;
  }
}

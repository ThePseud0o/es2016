#include <Servo.h>

int ledPin = 13;
Servo myservo;

String input = "";
int angle = 90;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(3, OUTPUT);
  
  myservo.attach(3);
  myservo.write(angle);
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    input = Serial.readString();
    angle = input.substring(17).toInt();
    if(input == "moveServoToAngle(" + (String)angle + ")") {
      if(angle >= 0 && angle <= 180) {
        myservo.write(angle);
        Serial.println(angle);
        Serial.println("moveServoToAngle(" + (String)angle + ")");
       }
       else
       {
         blinkLed();
       }
    }
        input = "";
   }
}

void blinkLed() {
  for(int i = 0; i < 3; i++) {
   digitalWrite(ledPin, HIGH);
   delay(100);
   digitalWrite(ledPin, LOW);
   delay(50);
  }
}

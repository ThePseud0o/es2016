#include "duetimer.h"

int z, x, ref;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  configureTimer(7, readAndPrint, 10);
  startTimer(7);
}

void loop() {
}

void readAndPrint() {
  ref = analogRead(7);
  z = analogRead(8) - ref;
  z = map(z, -512, 511, -500, 500);
  x = analogRead(1) - ref;
  x = map(x, -512, 511, -500, 500);
  Serial.println("x: " + (String)x + ", z: " + (String)z);
}

int ledPin = 7;
int btnPinL = 5;
int btnPinR = 3;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(btnPinL, INPUT);
  pinMode(btnPinR, INPUT);
}

void loop() {
  if(digitalRead(btnPinR) == LOW) {
    digitalWrite(ledPin, HIGH);
  }
  else {
    digitalWrite(ledPin, LOW);
  }
}


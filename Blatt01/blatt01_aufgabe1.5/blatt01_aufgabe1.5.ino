int ledPin = 7;
int btnPinL = 5;
int btnPinR = 3;
int brightness = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(btnPinL, INPUT);
  pinMode(btnPinR, INPUT);
  attachInterrupt(btnPinR, plus, FALLING);
  attachInterrupt(btnPinL, minus, FALLING);
  brightness = 0;
  Serial.begin(9600);
}

void loop() {
  analogWrite(ledPin, brightness);
  Serial.println(brightness);
}

void plus() {
    brightness += 16;
    if(brightness > 255)
      brightness = 255;
}

void minus(){
    brightness -= 16;
    if(brightness < 0)
      brightness = 0;
}


int ledPin = 7;
int btnPinL = 5;
int btnPinR = 3;
boolean isOn = false;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(btnPinL, INPUT);
  pinMode(btnPinR, INPUT);
  attachInterrupt(btnPinR, change, FALLING);
}

void loop() {
}

void change() {
    	if(isOn) {
    		turnOff();
    	} else {
    		turnOn();
    	}
    isOn = !isOn;
}

void turnOn() {
    digitalWrite(ledPin, HIGH);	
}

void turnOff(){
    digitalWrite(ledPin, LOW);
}


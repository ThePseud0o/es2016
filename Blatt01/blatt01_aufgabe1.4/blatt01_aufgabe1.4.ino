int ledPin = 7;
int btnPinL = 5;
int btnPinR = 3;
int ms;
int dBlink = 5;

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  ms = ((int)(millis()/1/dBlink)) % 256;
  analogWrite(ledPin, ms);
  Serial.print("Wert: ");
  Serial.print(ms);
  Serial.println();
  delay(10);
}

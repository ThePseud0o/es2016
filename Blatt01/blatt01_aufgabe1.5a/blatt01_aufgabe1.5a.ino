int ledPin = 7;
int btnPinL = 5;
int btnPinR = 3;
int brightness = 0;
boolean prvBtnL = false;
boolean prvBtnR = false;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(btnPinL, INPUT);
  pinMode(btnPinR, INPUT);
  brightness = 0;
  Serial.begin(9600);
}

void loop() {
  if (!digitalRead(btnPinL) && !prvBtnL){
    prvBtnL = true;
    plus();
  }
  if (!digitalRead(btnPinR) && !prvBtnR){
    prvBtnR = true;
    minus();
  }
  if (digitalRead(btnPinL)){
    prvBtnL = false;
  }
  if (digitalRead(btnPinR)){
    prvBtnR = false;
  }
  analogWrite(ledPin, brightness);
}

void plus() {
    brightness += 16;
    if(brightness > 255)
      brightness = 255;
}

void minus(){
    brightness -= 16;
    if(brightness < 0)
      brightness = 0;
}


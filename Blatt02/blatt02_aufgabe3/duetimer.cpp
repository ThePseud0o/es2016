#include <Arduino.h>
#include <inttypes.h>
#include "duetimer.h"

#define DUE_F_CPU 84000000

typedef struct
{
    TcChannel *tc_channel;
    uint32_t pmc_id;
    IRQn nvic_irqn;
    void (*callback)(void);
} DueTimer;

DueTimer timers[9] = { { TC0->TC_CHANNEL, ID_TC0, TC0_IRQn, NULL },
                       { TC0->TC_CHANNEL+1, ID_TC1, TC1_IRQn, NULL },
                       { TC0->TC_CHANNEL+2, ID_TC2, TC2_IRQn, NULL },
                       { TC1->TC_CHANNEL, ID_TC3, TC3_IRQn, NULL },
                       { TC1->TC_CHANNEL+1, ID_TC4, TC4_IRQn, NULL },
                       { TC1->TC_CHANNEL+2, ID_TC5, TC5_IRQn, NULL },
                       { TC2->TC_CHANNEL, ID_TC6, TC6_IRQn, NULL },
                       { TC2->TC_CHANNEL+1, ID_TC7, TC7_IRQn, NULL },
                       { TC2->TC_CHANNEL+2, ID_TC8, TC8_IRQn, NULL } };

bool configureTimer(const uint8_t timer_id, void (*timer_callback)(void),
                    const uint32_t timer_hz)
{
    if (timer_id > 8) { return false; }

    timers[timer_id].callback = timer_callback;

    pmc_set_writeprotect(false);
    pmc_enable_periph_clk(timers[timer_id].pmc_id);

    // Timer uses smallest available divisor (i.e. 2) for the master clock
    timers[timer_id].tc_channel->TC_CMR =
        TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK1;

    // Compare value is calculated based on the master clock divisor
    uint32_t cmp_value = (DUE_F_CPU / 2 / timer_hz) - 1;
    timers[timer_id].tc_channel->TC_RC = cmp_value;

    timers[timer_id].tc_channel->TC_IER = TC_IER_CPCS;
    timers[timer_id].tc_channel->TC_IDR = ~TC_IDR_CPCS;

    NVIC_ClearPendingIRQ(timers[timer_id].nvic_irqn);
    NVIC_EnableIRQ(timers[timer_id].nvic_irqn);

    return true;
}

bool startTimer(const uint8_t timer_id)
{
    if (timer_id > 8) { return false; }
    if (timers[timer_id].callback == NULL) { return false; }

    timers[timer_id].tc_channel->TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG;

    return true;
}

bool stopTimer(const uint8_t timer_id)
{
    if (timer_id > 8) { return false; }

    timers[timer_id].tc_channel->TC_CCR = TC_CCR_CLKDIS;

    return true;
}

void TC0_Handler(void)
{
    uint8_t index = 0;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC1_Handler(void)
{
    uint8_t index = 1;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC2_Handler(void)
{
    uint8_t index = 2;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC3_Handler(void)
{
    uint8_t index = 3;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC4_Handler(void)
{
    uint8_t index = 4;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC5_Handler(void)
{
    uint8_t index = 5;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC6_Handler(void)
{
    uint8_t index = 6;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC7_Handler(void)
{
    uint8_t index = 7;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

void TC8_Handler(void)
{
    uint8_t index = 8;
    uint32_t volatile value = timers[index].tc_channel->TC_SR;
    timers[index].callback();
}

#include <inttypes.h>
#include "duetimer.h"

int pwmPin = 2;
int in1Pin = 3;
int in2Pin = 4;
int standbyPin = 5;
int btnPinL = 11;
int btnPinR = 12;
int rotation = 0;
int rotationType = 0;
boolean prvBtnL = false;
boolean prvBtnR = false;
int counterBtnL = 0;
int counterBtnR = 0;
int step = 15;

void setup() {
  pinMode(in1Pin, OUTPUT);
  pinMode(in2Pin, OUTPUT);
  pinMode(pwmPin, OUTPUT);
  pinMode(standbyPin, OUTPUT);
  pinMode(btnPinL, INPUT);
  pinMode(btnPinR, INPUT);
  rotation = 0;
  btnPressedL();
  Serial.begin(9600);
  configureTimer(7, checkButtons, 1000);
  startTimer(7);
}

void checkButtons() {
  if (!digitalRead(btnPinL) && !prvBtnL
      || digitalRead(btnPinL) && prvBtnL){
    counterBtnL = 0;
  } else {
    counterBtnL++;
    if (counterBtnL == 10){
      prvBtnL = !prvBtnL;
      if(!prvBtnL)
        btnPressedL();
    }
  }
  if ((!digitalRead(btnPinR) && !prvBtnR)
      || (digitalRead(btnPinR) && prvBtnR)){
    counterBtnR = 0;
  } else {
    counterBtnR++;
    if (counterBtnR == 10){
      prvBtnR = !prvBtnR;
      if(!prvBtnR)
        btnPressedR();
    }
  }
}

void btnPressedL(){
  rotationType++;
  rotationType %= 4;
  switch(rotationType) {
    case (0):
      digitalWrite(in1Pin, 0);
      digitalWrite(in2Pin, 0);
      break;
    case (1):
      digitalWrite(in1Pin, 1);
      digitalWrite(in2Pin, 0);
      break;
    case (2):
      digitalWrite(in1Pin, 0);
      digitalWrite(in2Pin, 0);
      break;
    case (3):
      digitalWrite(in1Pin, 0);
      digitalWrite(in2Pin, 1);
  }
}

void btnPressedR(){
  rotation += step;
  if(rotation >= 255) {
    rotation = 255;
    step *= -1;
  }
  if(rotation <= 0) {
    rotation = 0;
    step *= -1;
  }
}

void loop() {
  analogWrite(pwmPin, rotation);
}

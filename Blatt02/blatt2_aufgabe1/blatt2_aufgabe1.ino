#include <inttypes.h>
#include "duetimer.h"

uint8_t led_state = LOW;

void changeLedState(void)
{
    led_state = !led_state;
    digitalWrite(13, led_state);
}

void setup()
{
	pinMode(13, OUTPUT);
	configureTimer(0, changeLedState, 10);
	startTimer(0);
}

void loop()
{
	//
}

#include <inttypes.h>
#include "duetimer.h"

int ledPin = 7;
int btnPinL = 5;
int btnPinR = 3;
int brightness = 0;
boolean prvBtnL = false;
boolean prvBtnR = false;
int counterBtnL = 0;
int counterBtnR = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(btnPinL, INPUT);
  pinMode(btnPinR, INPUT);
  brightness = 0;
  Serial.begin(9600);
  configureTimer(0, checkButtons, 1000);
  startTimer(0);
}

void checkButtons() {
  if (!digitalRead(btnPinL) && !prvBtnL
      || digitalRead(btnPinL) && prvBtnL){
    counterBtnL = 0;
  } else {
    counterBtnL++;
    if (counterBtnL == 10){
      prvBtnL = !prvBtnL;
      if(!prvBtnL)
        plus();
    }
  }
  if ((!digitalRead(btnPinR) && !prvBtnR)
      || (digitalRead(btnPinR) && prvBtnR)){
    counterBtnR = 0;
  } else {
    counterBtnR++;
    if (counterBtnR == 10){
      prvBtnR = !prvBtnR;
      if(!prvBtnR)
        minus();
    }
  }
}

void loop() {
  analogWrite(ledPin, brightness);
}

void plus() {
    brightness += 15;
    if(brightness > 255)
      brightness = 255;
}

void minus(){
    brightness -= 15;
    if(brightness < 0)
      brightness = 0;
}


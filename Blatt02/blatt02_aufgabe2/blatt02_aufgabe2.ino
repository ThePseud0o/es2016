#include <inttypes.h>
#include "duetimer.h"

int pwmPin = 2;
int in1Pin = 3;
int in2Pin = 4;
int standbyPin = 5;
int rotation = 0;
boolean clockwise = true;

void setup() {
  rotation = 0;
  pinMode(in1Pin, OUTPUT);
  pinMode(in2Pin, OUTPUT);
  pinMode(pwmPin, OUTPUT);
  pinMode(standbyPin, OUTPUT);
  digitalWrite(in1Pin, clockwise);
  digitalWrite(in2Pin, !clockwise);
  digitalWrite(standbyPin, HIGH);
  Serial.begin(9600);
  configureTimer(7, rotate, 51);
  configureTimer(8, halt, 255);
  startTimer(7);
}

void rotate(){
  rotation++;
  Serial.println(rotation);
  Serial.println(clockwise);
  if(rotation > 254){
    stopTimer(7);
    startTimer(8);
  }
}

void halt(){
  rotation--;
  if (rotation < 1){
    clockwise = !clockwise;
    digitalWrite(in1Pin, clockwise);
    digitalWrite(in2Pin, !clockwise);
    stopTimer(8);
    startTimer(7);
  }
}

void loop() {
  analogWrite(pwmPin, rotation);
}
